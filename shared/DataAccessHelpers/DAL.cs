﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;

namespace DataAccess
{
    public class DAL
    {
        //Fields and properties
        protected internal string connectionString;

        //Constructor
        public DAL(string connString)
        {
            connectionString = connString;
        }

        //Basic Operations
        public object ExecuteScalar(string sql)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(sql, con))
                {
                    con.Open();
                    cmd.CommandTimeout = 300000;
                    object result = cmd.ExecuteScalar();
                    con.Close();
                    return result;
                }
            }
        }

        public DataTable ExecuteQuery(string sql)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(sql, con))
                {
                    DataTable dt = new DataTable();
                    con.Open();
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        cmd.CommandTimeout = 300000;
                        da.SelectCommand = cmd;
                        da.Fill(dt);
                    }                    
                    con.Close();
                    return dt;
                }
            }
        }

        public int ExecuteNonQuery(string sql)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(sql, con))
                {
                    int nRowAffected = 0;
                    con.Open();
                    cmd.CommandTimeout = 300000;
                    nRowAffected = cmd.ExecuteNonQuery();
                    con.Close();
                    return nRowAffected;
                }
            }
        }

        /// <summary>
        /// Insert file to database
        /// string fileName = "C:\\IntelChipset.log";
        /// FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
        /// BinaryReader br = new BinaryReader(fs);
        /// int numBytes = (int)(new FileInfo(fileName)).Length;        
        /// byte[] buff = br.ReadBytes(numBytes);
        /// sql = "INSERT INTO Log(Id,LogFile) VALUES (1,@LogFile);";
        /// dictionary.Add("@LogFile",byte[]);
        /// 
        /// Query file from database
        /// System.Data.DataTable dt = dao.ExecuteQuery("SELECT TOP 1 * FROM Log");
        /// byte[] buff2 = (byte[])dt.Rows[0]["LogFile"];
        /// File.WriteAllBytes("C:\\test.txt", buff2);
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="dict"></param>
        public void InsertBytes(string sql, Dictionary<string, byte[]> dict /*fieldname,byte[]*/)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(sql, con))
                {
                    SqlParameter param = null;
                    foreach (KeyValuePair<string, byte[]> kvp in dict)
                    {
                        param = cmd.Parameters.Add(kvp.Key, SqlDbType.VarBinary);
                        param.Value = kvp.Value;
                    }                  
                    cmd.ExecuteNonQuery();
                }
            }
        }

        //Transaction Operations--------------------------------------------------------------
        protected internal SqlConnection connection;
        private SqlCommand command;
        private SqlTransaction transaction;
        public void BeginTransaction(IsolationLevel level)
        {
            try
            {
                connection.Open();
                command = connection.CreateCommand();
                // Start a local transaction.
                transaction = connection.BeginTransaction(level);
                // Must assign both transaction object and connection
                // to Command object for a pending local transaction
                command.Connection = connection;
                command.Transaction = transaction;
            }
            catch (Exception ex)
            {
                if (command != null) command.Dispose();
                if (connection != null) connection.Close();
                throw ex;
            }
        }

        public void ExecuteTransaction(string sql)
        {
            try
            {
                command.CommandText = sql;
                command.CommandTimeout = 300000;
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                command.Dispose();
                connection.Close();
                throw ex;
            }
        }

        public void CommitTransaction()
        {
            try
            {
                // Attempt to commit the transaction.
                transaction.Commit();
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }

        public static void UpdateUntilComplete(Action updateMethod)
        {
            while (true)
            {
                try
                {
                    updateMethod.Invoke();
                    break;
                }
                catch
                {
                    //Loop until update successfully
                    System.Threading.Thread.Sleep(5 * 60 * 1000);
                }
            }
        }
    }
}